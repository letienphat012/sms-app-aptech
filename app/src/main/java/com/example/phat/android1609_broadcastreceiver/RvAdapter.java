package com.example.phat.android1609_broadcastreceiver;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Phat on 2017-09-21.
 */

public class RvAdapter extends RecyclerView.Adapter<RvAdapter.VH>{
    private RecyclerView rv;
    private Context context;
    private ArrayList<Data> listData;
    private CustomOnItemClick clickListener;


    public RvAdapter(ArrayList<Data> listData,Context ctx, CustomOnItemClick clickListener,RecyclerView rv) {
        this.rv=rv;
        this.context=ctx;
        this.listData = listData;
        this.clickListener = clickListener;
    }




    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item,parent,false);
        final VH vh = new VH(v);
//        v.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                clickListener.onItemClick(v,vh.getAdapterPosition());
//
//            }
//        });
        v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                clickListener.onItemClick(v,vh.getAdapterPosition());
                return true;
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        //chỉ animate khi pos = 0 vì add vào đầu list khi có tin nhắn mới tới


        if(position==0){

                Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
                holder.itemView.startAnimation(animation);


        }

        String name = listData.get(position).getName();
        if (name==null){
            holder.tvAddress.setText(listData.get(position).getAddress());
        }
        else{
            holder.tvAddress.setText(listData.get(position).getName());
        }
        Bitmap img = listData.get(position).getImg();
        holder.ivIcon.setImageBitmap(img);

        holder.tvMsg.setText(listData.get(position).getMessage());
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public static class VH extends RecyclerView.ViewHolder{
        TextView tvMsg;
        TextView tvAddress;
        ImageView ivIcon;
        public VH(View itemView) {
            super(itemView);
            tvMsg= (TextView) itemView.findViewById(R.id.tvMsg);
            tvAddress= (TextView) itemView.findViewById(R.id.tvAddress);
            ivIcon = (ImageView) itemView.findViewById(R.id.ivAvatar);
        }
    }
}