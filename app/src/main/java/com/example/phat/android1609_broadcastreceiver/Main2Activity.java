package com.example.phat.android1609_broadcastreceiver;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Random;

public class Main2Activity extends AppCompatActivity implements CustomOnItemClick {
    public final static String TAG ="AAAA";
    private ArrayList<Data> data;
    private RecyclerView rvMain;
    private RvAdapter rvAdapter;
    private Animation anima;
    private MySQLHelper sqliteHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main2);
        setTitle("List Msg");
        data= new ArrayList<>();
        rvMain = (RecyclerView) findViewById(R.id.rvMsg);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rvMain.setLayoutManager(llm);
        rvAdapter = new RvAdapter(data,this,this,rvMain);

        rvMain.setAdapter(rvAdapter);

        //Run first time
        //lấy dữ liệu từ broadcast
        receiveFirstMsg();
    }

    public void receiveFirstMsg(){
        Bundle b = getIntent().getBundleExtra("msgInfo");
        String addr = b.getString("address", "none");
        String msg = b.getString("Msg", "none");

        sqliteHelper = new MySQLHelper(this);
        SQLiteDatabase db = sqliteHelper.getWritableDatabase();


//        String name = sqliteHelper.getNameByPhone(addr);
        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(addr));
        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
        String name="";
        Cursor cursor= getContentResolver().query(uri,projection,null,null,null);
        if (cursor != null) {
            if(cursor.moveToFirst()) {
                name=cursor.getString(0);//đã lấy được name
            }
            cursor.close();
        }
        Bitmap img;
        try{
             img= openPhoto(getContactIDFromNumber(addr,this));
        }catch (Exception e){
            img=null;
        }


        if(name ==null ||name.equals("")){
            Data d = new Data(msg,addr,"",img);
            data.add(0,d);
            sqliteHelper.insert(addr);
        }
        else{
            Data d = new Data(msg,addr,name,img);
            data.add(0,d);
        }
        rvAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle b = intent.getBundleExtra("msgInfo");
        String addr = b.getString("address", "none");
        String msg = b.getString("Msg", "none");

        //get name tu database
//        String name = sqliteHelper.getNameByPhone(addr);
        Bitmap img;
        try{
            img= openPhoto(getContactIDFromNumber(addr,this));
        }catch (Exception e){
            img=null;
        }
        //get name tu phone trong danh ba
        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(addr));
        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
        String name="";
        Cursor cursor= getContentResolver().query(uri,projection,null,null,null);
        if (cursor != null) {
            if(cursor.moveToFirst()) {
                name=cursor.getString(0);//đã lấy được name

            }
            cursor.close();
        }

        if(name ==null ||name.equals("")){
            Data d = new Data(msg,addr,"",img);


//            sqliteHelper.insert(addr);

            if(data.contains(d)){
                data.remove(data.indexOf(d));
                data.add(0,d);
            }
            else{
                data.add(0,d);
            }
        }
        else{
            Data d = new Data(msg,addr,name,img);
            if(data.contains(d)){
                data.remove(data.indexOf(d));
                data.add(0,d);
            }
            else{
                data.add(0,d);
            }
        }

        rvAdapter.notifyDataSetChanged();

    }

    @Override
    public void onItemClick(View v, int position) {
        Intent iContactDetail = new Intent(Main2Activity.this,ContactDetail.class);
        String phoneNum = data.get(position).getAddress();
        iContactDetail.putExtra("phoneNum",phoneNum);
        String msg=data.get(position).getMessage();
        iContactDetail.putExtra("msg",msg);
//        if(ContactDetail.isActive){

//        }
//        else{
            startActivity(iContactDetail);

//        }


    }

    public void onSeeConTactListClick(View view) {
        Intent iContactList = new Intent(Main2Activity.this,ContactList.class);
        startActivity(iContactList);

    }
    public Bitmap openPhoto(long contactId) {
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
        Cursor cursor = getContentResolver().query(photoUri,
                new String[] {ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
        if (cursor == null) {
            return null;
        }
        try {
            if (cursor.moveToFirst()) {
                byte[] data = cursor.getBlob(0);
                if (data != null) {
                    return BitmapFactory.decodeStream(new ByteArrayInputStream(data));
                }
            }
        } finally {
            cursor.close();
        }
        return null;

    }
    public static long getContactIDFromNumber(String contactNumber, Context context) {
        String UriContactNumber = Uri.encode(contactNumber);
        long phoneContactID = new Random().nextInt();
        Cursor contactLookupCursor = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, UriContactNumber),
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID}, null, null, null);
        while (contactLookupCursor.moveToNext()) {
            phoneContactID = contactLookupCursor.getLong(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
        }
        contactLookupCursor.close();

        return phoneContactID;
    }
}


