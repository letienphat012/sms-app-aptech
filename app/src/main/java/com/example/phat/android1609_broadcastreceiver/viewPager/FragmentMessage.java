package com.example.phat.android1609_broadcastreceiver.viewPager;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AlertDialogLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.phat.android1609_broadcastreceiver.CustomOnItemClick;
import com.example.phat.android1609_broadcastreceiver.Data;
import com.example.phat.android1609_broadcastreceiver.MySQLHelper;
import com.example.phat.android1609_broadcastreceiver.R;
import com.example.phat.android1609_broadcastreceiver.RvAdapter;
import com.example.phat.android1609_broadcastreceiver.SendMessageActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Phat on 2017-10-02.
 */

public class FragmentMessage extends Fragment implements CustomOnItemClick {
    public static final String TAG = "LTP";
    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    private RecyclerView rvMsgList;
    private ArrayList<Data> listData;
    private RvAdapter adapter;
    private ProgressBar pbProgress;
    private SwipeRefreshLayout srlSwipe;
    private Button btnPhone;
    private Button btnSMS;

    //Factory
    public static FragmentMessage newInstance(String message){
        FragmentMessage f = new FragmentMessage();
        Bundle bdl = new Bundle();
        bdl.putString(EXTRA_MESSAGE,message);
        f.setArguments(bdl);
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.message_list_fragment,container,false);
        return v;
    }

    LoadMesssage lm;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pbProgress = (ProgressBar) view.findViewById(R.id.pbProgress);
        rvMsgList= (RecyclerView) view.findViewById(R.id.rvMsgList);
//        rvMsgList.setVisibility(View.INVISIBLE);
        srlSwipe = (SwipeRefreshLayout)view.findViewById(R.id.srlSwipe);

        pbProgress.setVisibility(View.VISIBLE);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        rvMsgList.setLayoutManager(llm);
        listData = new ArrayList<>();
        adapter = new RvAdapter(listData,getContext(),this,rvMsgList);
        rvMsgList.setAdapter(adapter);

        srlSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                srlSwipe.setRefreshing(true);
                adapter.notifyDataSetChanged();
                srlSwipe.setRefreshing(false);
            }

        });
        lm = new LoadMesssage();
        lm.execute();

        ContentResolverQuery query = new ContentResolverQuery();
        query.getAllContactInformation(getContext());
        Log.i(TAG, "onViewCreated: load completed!");

    }

    class LoadMesssage extends AsyncTask<Void,Integer,Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            rvMsgList.setVisibility(View.VISIBLE);
            pbProgress.setVisibility(View.INVISIBLE);
            adapter.notifyDataSetChanged();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);


        }

        @Override
        protected Void doInBackground(Void... params) {
            MySQLHelper sql = new MySQLHelper(getContext());
            List<Data> listMsg = sql.getNewestMessageByPhone();
            for (Data d:listMsg){
                try {

                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.i(TAG, "run: add"+d);
                listData.add(d);
            }
            return null;
        }
    }
    @Override
    public void onItemClick(View v, final int position) {
        AlertDialog.Builder alBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_layout,null);
        btnPhone=(Button)view.findViewById(R.id.btnPhone);
        btnSMS = (Button) view.findViewById(R.id.btnSMS);
        btnPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent call = new Intent(Intent.ACTION_CALL);
                call.setData(Uri.parse("tel:"+listData.get(position).getAddress()));
                startActivity(call);
            }
        });
        btnSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNum = listData.get(position).getAddress();
                Intent iSendMessage = new Intent(getActivity(), SendMessageActivity.class);
                iSendMessage.putExtra("PhoneNumber",phoneNum);
                startActivity(iSendMessage);
            }
        });
        alBuilder.setView(view);
        AlertDialog al = alBuilder.create();
        al.show();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        String phoneNum = outState.getString("address");
        String msg = outState.getString("Msg");

        ContentResolverQuery cRQ = new ContentResolverQuery();
        long id = cRQ.getContactIDByPhoneNumber(phoneNum, getActivity());

        String name = cRQ.getNameByPhoneNumber(phoneNum, getActivity());
        if(name.equals("")) name=null;
//        Bitmap bitmap = cRQ.getImageByPhoneNumber(phoneNum,getActivity());
        Bitmap bitmap = cRQ.openPhoto(id, getActivity());
        Data d = new Data(msg,phoneNum,name,bitmap);
        Log.i(TAG, "onSaveInstanceState: "+listData);
//        if(listData.contains(d)){
//            listData.remove(listData.indexOf(d));
//            listData.add(0,d);
//        }
//        else{
//            listData.add(0,d);
//        }
        adapter.notifyDataSetChanged();
    }
}