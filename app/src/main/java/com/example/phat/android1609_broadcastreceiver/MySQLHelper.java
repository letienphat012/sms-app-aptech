package com.example.phat.android1609_broadcastreceiver;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Phat on 2017-09-28.
 */

public class MySQLHelper extends SQLiteOpenHelper {
    public static final String TAG ="LTP";
    private static String DBNAME = "ContactDB";
    private static int DBVERSION=1;
    private Context context;
    public MySQLHelper(Context context) {
        super(context, DBNAME, null, DBVERSION);
        this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table Contact(ID Integer primary key autoincrement,PhoneNumber Text unique)");
        db.execSQL("create table Message(ID Integer primary key autoincrement,ContactID Interger references Contact(ID) on update cascade, Message Text)");
    }

    public void insert(String phoneNumber){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues row = new ContentValues();
        row.put("PhoneNumber",phoneNumber);
        db.insert("Contact",null,row);
    }

    public void insertMessage(String message,String phone){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues row = new ContentValues();
        Cursor c = db.query("Contact", new String[]{"ID"}, "PhoneNumber=?", new String[]{phone}, null, null, null);
        int id = -1;
        if(c!=null && c.moveToFirst()){
            do{
                id = c.getInt(c.getColumnIndex("ID"));
                Log.i(TAG, "insertMessage: id with c"+id);
            }
            while(c.moveToNext());
            c.close();
        }
        //không tìm thấy id phone trong bảng Contact
        if (id==-1){
            insert(phone);
            Cursor c2 = db.query("Contact", new String[]{"ID"}, "PhoneNumber=?", new String[]{phone}, null, null, null);
            if(c2!=null && c2.moveToFirst()){
                do{
                    id = c2.getInt(c2.getColumnIndex("ID"));
                }
                while(c2.moveToNext());
                Log.i(TAG, "insertMessage: id with c2"+id);
                c2.close();
            }
            row.put("ContactID",id);
            row.put("Message",message);
            db.insert("Message",null,row);
            Log.i(TAG, "insertMessage: "+row);
        }else{
            row.put("ContactID",id);
            row.put("Message",message);
            db.insert("Message",null,row);
        }
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public List<Data> getNewestMessageByPhone(){
        List<Data> newestMsgList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.query("Message as m join Contact as c on m.ContactID = c.ID", new String[]{"ContactID", "Message","PhoneNumber"}, null, null, "ContactID", null, null);
        if (c!= null && c.moveToFirst()){
            do{
                String msg = c.getString(c.getColumnIndex("Message"));
                String phone = c.getString(c.getColumnIndex("PhoneNumber"));
                Data d = new Data(msg, phone);
                newestMsgList.add(d);

            }while (c.moveToNext());
        }
        return newestMsgList;
    }
}
