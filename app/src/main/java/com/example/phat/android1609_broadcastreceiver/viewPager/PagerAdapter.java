package com.example.phat.android1609_broadcastreceiver.viewPager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.List;

/**
 * Created by Phat on 2017-10-02.
 */

public class PagerAdapter extends FragmentPagerAdapter {
    List<Fragment> fragments;
    public PagerAdapter(FragmentManager fm,List<Fragment> fragments) {
        super(fm);
        this.fragments=fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0: return "MESSAGE";
            case 1: return "CONTACT";
            default: return "NONAME TITLE";
        }

    }
}
