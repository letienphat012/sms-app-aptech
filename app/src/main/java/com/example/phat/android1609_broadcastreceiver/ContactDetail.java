package com.example.phat.android1609_broadcastreceiver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ContactDetail extends AppCompatActivity {
    public static boolean isActive = false;
    private TextView tvName;
    private TextView tvPhone;
    private TextView tvMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isActive=true;
        setContentView(R.layout.activity_contact_detail);

        String phoneNum = getIntent().getStringExtra("phoneNum");

        String name = getIntent().getStringExtra("contactName");
//        MySQLHelper sqlHelper = new MySQLHelper(this);
        tvName = (TextView) findViewById(R.id.tvName);
        tvPhone = (TextView) findViewById(R.id.tvPhone);
        tvMsg = (TextView) findViewById(R.id.tvMsg);
//        String name = sqlHelper.getNameByPhone(phoneNum);
        String msg = getIntent().getStringExtra("msg");
        if(name==null ||name.equals("")){
            tvName.setText("Name: "+"Unknown");

        }
        else{
            tvName.setText("Name: "+name);
        }
        tvPhone.setText("Phone Number: "+phoneNum);
        tvMsg.setText("Message: "+msg);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        isActive=false;
    }
}