package com.example.phat.android1609_broadcastreceiver.viewPager;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.phat.android1609_broadcastreceiver.ContactData;
import com.example.phat.android1609_broadcastreceiver.R;

import java.io.ByteArrayInputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Phat on 2017-10-03.
 */

public class ContentResolverQuery {
    public long getContactIDByPhoneNumber(String phoneNumber,Context context){
        long contactID = 0L;


        Cursor cContactID = context.getContentResolver().query(
                Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(phoneNumber)),
                new String[]{ContactsContract.PhoneLookup._ID},
                null,null,null);
        if(cContactID != null && cContactID.moveToFirst()){
            do {
                contactID = cContactID.getLong(0);
            }while (cContactID.moveToNext());
        }
        cContactID.close();
        return contactID;
    }

    public String getNameByPhoneNumber(String phoneNumber,Context context){
        String result ="";
        Cursor cursor = context.getContentResolver().query(
                Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(phoneNumber)),
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null,
                null, null);
        if(cursor != null && cursor.moveToFirst()){
            do {
                result  = cursor.getString(0);
            }while (cursor.moveToNext());
            cursor.close();
        }
        return result;
    }

    public Bitmap getImageByPhoneNumber(String phoneNumber, Context context){
        long id = getContactIDByPhoneNumber(phoneNumber, context);

        Cursor cursor = context.getContentResolver().query(
                ContactsContract.Data.CONTENT_URI,//from
                new String[]{ContactsContract.Contacts.Photo.PHOTO},// select
                ContactsContract.Contacts.Photo.CONTACT_ID+"=?",//where
                new String[]{String.valueOf(1)},//where args
                null);

        if(cursor != null && cursor.moveToFirst()){
            do {
                byte[] data = cursor.getBlob(0);
                Log.i("LTP", "getImageByPhoneNumber: "+data);
                if(data!=null){
                    cursor.close();
                    return BitmapFactory.decodeStream(new ByteArrayInputStream(data));
                }
                else {
                    cursor.close();
                    return BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher_round);
                }
            }while (cursor.moveToNext());

        }else{
            cursor.close();
            return BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher_round);
        }
    }

    public Bitmap openPhoto(long contactId,Context context) {
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[] {ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
        if (cursor == null) {

            return null;
        }

        try {

            if (cursor.moveToFirst()) {
                byte[] data = cursor.getBlob(0);
                Log.i("LTP", "openPhoto: no");
                Log.i("LTP", "openPhoto:  cursor null"+data);
                if (data != null) {
                    return BitmapFactory.decodeStream(new ByteArrayInputStream(data));
                }
            }
        } finally {
            cursor.close();
        }
        return null;

    }

    public static long getContactIDFromNumber(String contactNumber, Context context) {
        String UriContactNumber = Uri.encode(contactNumber);
        long phoneContactID = 0L;
        Cursor contactLookupCursor = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, UriContactNumber),
                new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID}, null, null, null);
        while (contactLookupCursor.moveToNext()) {
            phoneContactID = contactLookupCursor.getLong(contactLookupCursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
        }
        contactLookupCursor.close();

        return phoneContactID;
    }

    public static void getAllContactInformation(Context context){
        Cursor cursor = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, new String[]{
                ContactsContract.Contacts._ID
        }, null, null, null);
        List<ContactData> listContact = new ArrayList<>();
        List<Long> listID= new ArrayList<>();
        if(cursor!= null && cursor.moveToFirst()){
            do{
                ContactData data = new ContactData();
                Cursor cursorContactDisplayName = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                        new String[]{ContactsContract.Contacts.DISPLAY_NAME}
                        , ContactsContract.Contacts._ID + "=?"
                        , new String[]{cursor.getLong(cursor.getColumnIndex(ContactsContract.Contacts._ID))+""}
                        , null);
                Cursor cursorContactPhoneNumber = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI
                        , new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER}, ContactsContract.Data.CONTACT_ID + "=?"
                        , new String[]{cursor.getLong(cursor.getColumnIndex(ContactsContract.Contacts._ID)) + ""}
                        , null);
                if(cursorContactDisplayName!= null && cursorContactDisplayName.moveToFirst() && cursorContactPhoneNumber!=null&&cursorContactPhoneNumber.moveToFirst()){
                    do {
                        String name = cursorContactDisplayName.getString(cursorContactDisplayName.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        String phoneNum = cursorContactPhoneNumber.getString(cursorContactPhoneNumber.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        Log.i("LTP", "getAllContactInformation: "+name +".."+phoneNum);
                    }while (cursorContactDisplayName.moveToNext() && cursorContactPhoneNumber.moveToNext());
                }
            }while (cursor.moveToNext());
        }
//        return listContact;
    }
}