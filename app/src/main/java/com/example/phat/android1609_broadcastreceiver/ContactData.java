package com.example.phat.android1609_broadcastreceiver;

import android.graphics.Bitmap;

import java.util.ArrayList;

/**
 * Created by Phat on 2017-09-30.
 */

public class ContactData {
    private String name;
    private String phone;
    private Bitmap img;
    private ArrayList<String> listMsg;
    public ArrayList<String> getListMsg() {
        return listMsg;
    }

    public void setListMsg(ArrayList<String> listMsg) {

        this.listMsg = listMsg;
    }

    public ContactData(String name, String phone, Bitmap img) {
        this.name = name;
        this.phone = phone;
        this.img = img;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }

    public ContactData() {
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public boolean addMsg(String msg){
        return listMsg.add(msg);
    }
}

