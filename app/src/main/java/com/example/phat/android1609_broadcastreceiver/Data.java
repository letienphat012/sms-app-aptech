package com.example.phat.android1609_broadcastreceiver;

import android.graphics.Bitmap;

/**
 * Created by Phat on 2017-09-19.
 */

public class Data {
    private String message;
    private String address;
    private String name;
    private Bitmap img;

    public Data(String message, String address, String name, Bitmap img) {
        this.message = message;
        this.address = address;
        this.name = name;
        this.img = img;
    }

    public Data(String message, String address) {
        this.message = message;
        this.address = address;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {

        this.name = name;
    }



    public String getMessage() {
        return message;
    }

    public String getAddress() {
        return address;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Data data = (Data) o;

        return address.equals(data.address);

    }

    @Override
    public int hashCode() {
        return address.hashCode();
    }
}
