package com.example.phat.android1609_broadcastreceiver;

import android.view.View;

/**
 * Created by Phat on 2017-09-21.
 */

public interface CustomOnItemClick {
    void onItemClick(View v,int position);
}
