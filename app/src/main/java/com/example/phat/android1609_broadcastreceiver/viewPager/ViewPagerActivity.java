package com.example.phat.android1609_broadcastreceiver.viewPager;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.phat.android1609_broadcastreceiver.MySQLHelper;
import com.example.phat.android1609_broadcastreceiver.R;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerActivity extends AppCompatActivity {
    public static final String TAG="LTP";
    private TabLayout tabLayout;
    private FragmentMessage fMessage;
    private FragmentContact fContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS)!= PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this,Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.RECEIVE_SMS
                    ,Manifest.permission.READ_CONTACTS
                    ,Manifest.permission.CALL_PHONE
                    ,Manifest.permission.SEND_SMS
            },0x0);

        }


        ViewPager viewPager= (ViewPager) findViewById(R.id.vpViewPager);
        tabLayout = (TabLayout) findViewById(R.id.tlTabLayout);
        //tạo tab
        tabLayout.addTab(tabLayout.newTab().setText("MESSAGE"));
        tabLayout.addTab(tabLayout.newTab().setText("CONTACT"));

        List<Fragment> fragments = getFragments();
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(),fragments);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

    }

    /**
     * tạo list fragment dùng cho adapter
     * @return
     */
    private List<Fragment> getFragments() {
        List<Fragment> fragments = new ArrayList<>();
        fMessage = FragmentMessage.newInstance("MESSAGE");
        fragments.add(fMessage);
        fContact = FragmentContact.newInstance("CONTACT");
        fragments.add(fContact);


        return fragments;
    }

    //khi có tin nhắn mới (start lại activity)
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        final Bundle bdlInfo = intent.getBundleExtra("msgInfo");
        //gọi event onSaveInstanceState của fragment
        fMessage.onSaveInstanceState(bdlInfo);
        final MySQLHelper sql = new MySQLHelper(this);
        //dùng thread lưu xuống database
        new Thread(new Runnable() {
            @Override
            public void run() {
                String phone = bdlInfo.getString("address");
                String msg = bdlInfo.getString("Msg");
                sql.insertMessage(msg,phone);

                sql.getNewestMessageByPhone();
            }
        }).start();

    }

}
