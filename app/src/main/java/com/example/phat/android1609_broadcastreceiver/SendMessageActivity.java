package com.example.phat.android1609_broadcastreceiver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class SendMessageActivity extends AppCompatActivity {

    private EditText etPhoneNumer;
    private EditText etMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);
        etPhoneNumer = (EditText)findViewById(R.id.etPhoneNum);
        etMessage = (EditText)findViewById(R.id.etMessage);
        String phoneNum = getIntent().getStringExtra("PhoneNumber");
        etPhoneNumer.setText(phoneNum);
    }

    public void onSendClick(View view) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(etPhoneNumer.getText()+"",null,etMessage.getText()+"",null,null);

        Log.i("LTP", "onSendClick: send SmS succeed!");
    }
}
