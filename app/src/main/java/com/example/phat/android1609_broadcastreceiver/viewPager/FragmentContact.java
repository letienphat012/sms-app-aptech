package com.example.phat.android1609_broadcastreceiver.viewPager;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.phat.android1609_broadcastreceiver.ContactData;
import com.example.phat.android1609_broadcastreceiver.ContactDetail;
import com.example.phat.android1609_broadcastreceiver.CustomOnItemClick;
import com.example.phat.android1609_broadcastreceiver.R;
import com.example.phat.android1609_broadcastreceiver.RvContactAdapter;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/**
 * Created by Phat on 2017-10-02.
 */

public class FragmentContact extends Fragment implements CustomOnItemClick {
    public static final String TAG = "LTP";
    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    private RecyclerView rvContact;
    private ArrayList<ContactData> listContact;
    //Factory
    public static FragmentContact newInstance(String message){
        FragmentContact f = new FragmentContact();
        Bundle bdl = new Bundle();
        bdl.putString(EXTRA_MESSAGE,message);
        f.setArguments(bdl);
        return f;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.contact_list_fragment,container,false);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listContact = new ArrayList<>();

        rvContact = (RecyclerView) view.findViewById(R.id.rvFragContact);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        rvContact.setLayoutManager(llm);


        //lấy tên hiển thị
        Cursor cursor = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                new String[]{ContactsContract.Contacts.DISPLAY_NAME},null,null,null);
        if (cursor != null) {
            if(cursor.moveToFirst()) {
                do{
                    String name=cursor.getString(0);//đã lấy được name
                    ContactData cData = new ContactData();
                    cData.setName(name);
                    listContact.add(cData);
                }while (cursor.moveToNext());
            }
            cursor.close();
        }
        //lấy phone number
        cursor = getActivity().getContentResolver().query(ContactsContract.Data.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},null,null,null);

        if(cursor!= null && cursor.moveToFirst()){
            int i =0;
            do{
                String phone = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            }while (cursor.moveToNext());
            cursor.close();
        }
            /*Lấy thử  1 photo với contact ID = 1*/
//        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, 1);/* content://com.android.contacts/contacts/1 */
//        String pUri = ContactsContract.Contacts.PHOTO_THUMBNAIL_URI;
//        Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY); /* content://com.android.contacts/contacts/1/photo */
//        cursor =getActivity().getContentResolver().query(photoUri,
//                new String[]{ContactsContract.Contacts.Photo.PHOTO},null,null,null);


            /* getAllPhoto(tuần tự)*/
            cursor = getActivity().getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                    new String[]{ContactsContract.Contacts._ID},null,null,null);//lấy tuần tự id của tất cả contact
        if(cursor!= null && cursor.moveToFirst()){
            int i = 0;
            do{//duyệt mỗi lần lấy 1 photo trong danh bạ, từ trên xuống dưới
                Cursor cursor2 = getActivity().getContentResolver().query(
                        ContactsContract.Data.CONTENT_URI,//from
                        new String[]{ContactsContract.Contacts.Photo.PHOTO},// select
                        ContactsContract.Contacts.Photo.CONTACT_ID+"=?",//where
                        new String[]{cursor.getLong(0)+""},//where args
                        null);
                //đã lấy được photo của CONTACT_ID đang duyệt
                if(cursor2!= null && cursor2.moveToFirst()){
                    do{
                        byte[] data = cursor2.getBlob(0);
                        if(data != null){//gán hình ảnh
                            listContact.get(i).setImg(BitmapFactory.decodeStream(new ByteArrayInputStream(data)));
                        }else{//gán hình ảnh mặc định nếu không có hình (default: ic_launcher_round)
                            listContact.get(i).setImg(BitmapFactory.decodeResource(getActivity().getResources(),R.mipmap.ic_launcher_round));
                        }

                    }while(cursor2.moveToNext());
                    cursor2.close();
                }
                i++;//tăng i theo vị trí object Contact trong arrayList (Recycler View) để gán cho đúng chỗ
            }while(cursor.moveToNext());
        }


        RvContactAdapter adapter = new RvContactAdapter(rvContact,getContext(),listContact,this);
        rvContact.setAdapter(adapter);

    }

    @Override
    public void onItemClick(View v, int position) {
        Intent iContactDetail = new Intent(getActivity(),ContactDetail.class);
        String phoneNum = listContact.get(position).getPhone();
        String contactName = listContact.get(position).getName();
        iContactDetail.putExtra("phoneNum",phoneNum);
        iContactDetail.putExtra("contactName",contactName);
        String uri = Uri.encode(phoneNum);

        startActivity(iContactDetail);

    }
}
