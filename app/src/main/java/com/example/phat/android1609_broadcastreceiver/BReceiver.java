package com.example.phat.android1609_broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.example.phat.android1609_broadcastreceiver.viewPager.ViewPagerActivity;

/**
 * Created by Phat on 2017-09-16.
 */

public class BReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent iListMsg = new Intent(context,ViewPagerActivity.class);
        Log.i("CONTEXT",context.getPackageName());
        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Bundle bundle = intent.getExtras();
            if(bundle!=null){
                Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage smg = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    Bundle bInfo = new Bundle();
                    bInfo.putString("Msg",smg.getDisplayMessageBody());
                    bInfo.putString("address",smg.getDisplayOriginatingAddress());
                    iListMsg.putExtra("msgInfo",bInfo);
                }
            }
        }

        context.startActivity(iListMsg);
    }
}
