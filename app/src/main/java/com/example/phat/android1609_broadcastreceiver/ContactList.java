package com.example.phat.android1609_broadcastreceiver;

import android.content.ContentUris;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;

public class ContactList extends AppCompatActivity implements CustomOnItemClick {

    private RecyclerView rvContactList;
    private ArrayList<ContactData> listData;
    private RvContactAdapter rvContactAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);
        rvContactList = (RecyclerView) findViewById(R.id.rvContactList);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rvContactList.setLayoutManager(llm);

        listData= new ArrayList<>();



//        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(addr));
//        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
//        String name="";
//        Cursor cursor= getContentResolver().query(uri,projection,null,null,null);
        Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,null,null,null,null);
        if (cursor != null) {
            if(cursor.moveToFirst()) {
                do{
                    String name=cursor.getString(0);//đã lấy được name
                    ContactData cData = new ContactData();
                    cData.setName(name);
                    listData.add(cData);
                }while (cursor.moveToNext());
            }
            cursor.close();
        }
        rvContactAdapter = new RvContactAdapter(rvContactList,this,listData,this);
        rvContactList.setAdapter(rvContactAdapter);
        //lấy photo
//        rvContactAdapter.notifyDataSetChanged();

//        InputStream is = ContactsContract.Contacts.openContactPhotoInputStream(getContentResolver(), Uri.parse(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
//        Bitmap photo = BitmapFactory.decodeStream(is);

//        Log.i("LTP", "onCreate: "+photo);

    }

    @Override
    public void onItemClick(View v, int position) {

    }
}
