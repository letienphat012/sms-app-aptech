package com.example.phat.android1609_broadcastreceiver;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Phat on 2017-09-30.
 */


public class RvContactAdapter extends RecyclerView.Adapter<RvContactAdapter.VH>{
    private RecyclerView rv;
    private Context context;
    private ArrayList<ContactData> listData;
    private CustomOnItemClick clickListener;


    public RvContactAdapter(RecyclerView rv, Context context, ArrayList<ContactData> listData, CustomOnItemClick clickListener) {
        this.rv = rv;
        this.context = context;
        this.listData = listData;
        this.clickListener = clickListener;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_item,parent,false);
        final VH vh = new VH(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onItemClick(v,vh.getAdapterPosition());
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        if(position==0){
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);

            holder.itemView.startAnimation(animation);
        }

        holder.ivPhoto.setImageBitmap(listData.get(position).getImg());
        holder.tvContactName.setText(listData.get(position).getName());


    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public static class VH extends RecyclerView.ViewHolder{
        TextView tvContactName;
        ImageView ivPhoto;
        public VH(View itemView) {
            super(itemView);
            tvContactName= (TextView) itemView.findViewById(R.id.tvContactName);
            ivPhoto= (ImageView) itemView.findViewById(R.id.ivContactAvatar);
        }
    }
}
